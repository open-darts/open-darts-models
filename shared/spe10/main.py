from darts.engines import value_vector, redirect_darts_output
from model import Model
import gzip, shutil
import numpy as np

# from darts.engines import set_num_threads
# set_num_threads(1)

# with open('SPE10_input_ori.txt', 'rb') as f_in:
#     with gzip.open('SPE10.txt.gz', 'wb') as f_out:
#         shutil.copyfileobj(f_in, f_out)

with gzip.open('SPE10.txt.gz', 'rb') as f_in:
    with open('SPE10_input.txt', 'wb') as f_out:
        shutil.copyfileobj(f_in, f_out)

class DataTS:
    dt_min: float
    omega: float
    eta: float
    dt_max: float
    tol_res: float
    tol_wel: float
    tol_sta: float
    max_it_nl: int
    def __init__(self, nc):
        self.eta = 1e20 * np.ones(nc)  # avoid limitation for changes

        # default values
        self.dt_min = 1e-2
        self.omega = 1
        self.dt_max = 365
        self.tol_res = 1e-2
        self.tol_wel_mult = 1
        self.tol_sta = 1e-2
        self.max_it_nl = 12

redirect_darts_output('SPE10.log')
m = Model()
m.init()
data_dt = DataTS(m.physics.n_vars)
# data_dt.eta[:] = 0.6
# data_dt.eta[0] = 100
data_dt.dt_min = 1e-1
data_dt.tol_res = 1e-0

m.set_sim_params(first_ts=-1, mult_ts=-1, max_ts=-1, tol_newton=data_dt.tol_res, tol_linear=1e-2, it_newton=-1)
NT = 10
for i in range(NT):
    m.run(data_dt, 100, verbose=True)
m.print_timers()
m.print_stat()

for i in range(NT+1):
    m.output_to_vtk(ith_step=i)

