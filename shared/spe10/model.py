from darts.reservoirs.struct_reservoir import StructReservoir
from darts.models.darts_model import DartsModel
from darts.engines import value_vector
from darts.tools.keyword_file_tools import load_single_keyword
import numpy as np

from darts.physics.super.physics import Compositional
from darts.physics.super.property_container import PropertyContainer

from darts.physics.properties.basic import ConstFunc, PhaseRelPerm
from darts.physics.properties.density import DensityBasic

from math import fabs

class Model(DartsModel):
    def __init__(self):
        # call base class constructor
        super().__init__()

        # measure time spend on reading/initialization
        self.timer.node["initialization"].start()

        self.set_reservoir()
        self.set_physics()

        self.timer.node["initialization"].stop()

        self.initial_values = {self.physics.vars[0]: 413.6854,
                               self.physics.vars[1]: self.ini,
                               }

    def set_reservoir(self):
        cache = True
        permx = load_single_keyword('SPE10_input.txt', 'PERMX', cache=cache)
        permz = load_single_keyword('SPE10_input.txt', 'PERMZ', cache=cache)
        poro = load_single_keyword('SPE10_input.txt', 'PORO', cache=cache)
        depth = load_single_keyword('SPE10_input.txt', 'DEPTH', cache=cache)

        nz = 85
        nb = 60 * 220 * nz
        self.reservoir = StructReservoir(self.timer, nx=60, ny=220, nz=nz, dx=6.096, dy=3.048, dz=0.6096,
                                         permx=permx[:nb], permy=permx[:nb], permz=permz[:nb], poro=poro[:nb],
                                         depth=depth[:nb])
        return

    def set_wells(self):
        self.reservoir.add_well("I1")
        for k in range(self.reservoir.nz):
            self.reservoir.add_perforation("I1", cell_index=(30, 110, k+1), multi_segment=False)

        self.reservoir.add_well("P1")
        for k in range(self.reservoir.nz):
            self.reservoir.add_perforation("P1", cell_index=(1, 1, k+1), multi_segment=False)

        self.reservoir.add_well("P2")
        for k in range(self.reservoir.nz):
            self.reservoir.add_perforation("P2", cell_index=(60, 1, k+1), multi_segment=False)

        self.reservoir.add_well("P3")
        for k in range(self.reservoir.nz):
            self.reservoir.add_perforation("P3", cell_index=(1, 220, k+1), multi_segment=False)

        self.reservoir.add_well("P4")
        for k in range(self.reservoir.nz):
            self.reservoir.add_perforation("P4", cell_index=(60, 220, k+1), multi_segment=False)


    def set_physics(self):
        """Physical properties"""
        zero = 1e-13
        components = ["w", "o"]
        phases = ["wat", "oil"]

        self.ini = value_vector([0.2357])
        self.inj = value_vector([1 - 1e-6])

        property_container = ModelProperties(phases_name=phases, components_name=components, min_z=zero/10)

        property_container.density_ev = dict([('wat', DensityBasic(compr=4.5e-5, dens0=1025.18)),
                                              ('oil', DensityBasic(compr=2e-5, dens0=848.979))])
        property_container.viscosity_ev = dict([('wat', ConstFunc(0.3)),
                                                ('oil', ConstFunc(3))])
        property_container.rel_perm_ev = dict([('wat', PhaseRelPerm("gas", 0.2, 0.2)),
                                               ('oil', PhaseRelPerm("oil", 0.2, 0.2))])

        #property_container.rock_compr_ev =

        # create physics
        self.physics = Compositional(components, phases, self.timer,
                                     n_points=400, min_p=0, max_p=1000, min_z=zero, max_z=1-zero)
        self.physics.add_property_region(property_container)

        return

    def set_well_controls(self):

        for i, w in enumerate(self.reservoir.wells):
            if i == 0:
                w.control = self.physics.new_rate_inj(8e4, self.inj, 0)  # should be 794.9365 m3/day
                w.constraint = self.physics.new_bhp_inj(689.4757, self.inj)
            else:
                w.control = self.physics.new_bhp_prod(275.7903)

    def run(self, data_ts, days, restart_dt: float = 0., save_well_data: bool = True,
            save_solution_data: bool = True, verbose: bool = True):
        """
        Method to run simulation for specified time. Optional argument to specify dt to restart simulation with.

        :param days: Time increment [days]
        :type days: float
        :param restart_dt: Restart value for timestep size [days, optional]
        :type restart_dt: float
        :param verbose: Switch for verbose, default is True
        :type verbose: bool
        :param save_well_data: if True save states of well blocks at every time step to 'well_data.h5', default is True
        :type save_well_data: bool
        :param save_solution_data: if True save states of all reservoir blocks at the end of run to 'solution.h5', default is True
        :type save_solution_data: bool
        """
        days = days

        # get current engine time
        t = self.physics.engine.t
        stop_time = t + days

        # same logic as in engine.run
        if fabs(t) < 1e-15:
            dt = data_ts.dt_min
        elif restart_dt > 0.:
            dt = restart_dt
        else:
            dt = min(self.dt_prev, data_ts.dt_max)

        self.dt_prev = dt

        ts = 0

        nc = self.physics.n_vars
        nb = self.reservoir.mesh.n_res_blocks
        max_x = np.zeros(nc)

        while t < stop_time:
            xn = np.array(self.physics.engine.Xn[:nb * nc])
            converged = self.run_timestep(data_ts, dt, t, verbose)

            if converged:
                t += dt
                self.physics.engine.t = t
                ts += 1

                x = np.array(self.physics.engine.X[:nb * nc])
                dt_mult_new = 1e3
                for i in range(nc):
                    max_x[i] = np.max(abs(xn[i::nc] - x[i::nc]))
                    mult = ((1 + data_ts.omega) * data_ts.eta[i]) / (max_x[i] + data_ts.omega * data_ts.eta[i])
                    if mult < dt_mult_new:
                        dt_mult_new = mult

                if verbose:
                    print("# %d \tT = %10g\tDT = %8.4g\tNI = %2d\tLI=%3d\tdX=%4s\tmt=%3.3g"
                          % (ts, np.round(t, 3), np.round(dt, 4), self.physics.engine.n_newton_last_dt,
                             self.physics.engine.n_linear_last_dt, np.round(max_x, 3), dt_mult_new))

                dt = min(dt * dt_mult_new, data_ts.dt_max)

                if t + dt >= stop_time:
                    if t < stop_time:
                        self.dt_prev = min(dt, days)
                    dt = stop_time - t

                if save_well_data:
                    self.save_data_to_h5(kind='well')

            else:
                mult = (1 + data_ts.omega) / data_ts.omega
                dt /= mult
                if verbose:
                    print("Cut timestep to %2.10f" % dt)
                if dt < data_ts.dt_min:
                    break

        # update current engine time
        self.physics.engine.t = stop_time

        # save solution vector
        if save_solution_data:
            self.save_data_to_h5(kind='solution')

        if verbose:
            print("TS = %d(%d), NI = %d(%d), LI = %d(%d)"
                  % (self.physics.engine.stat.n_timesteps_total, self.physics.engine.stat.n_timesteps_wasted,
                     self.physics.engine.stat.n_newton_total, self.physics.engine.stat.n_newton_wasted,
                     self.physics.engine.stat.n_linear_total, self.physics.engine.stat.n_linear_wasted))


    def run_timestep(self, data_ts, dt: float, t: float, verbose: bool = True):
        """
        Method to solve Newton loop for specified timestep

        :param dt: Timestep size [days]
        :type dt: float
        :param t: Current time [days]
        :type t: float
        :param verbose: Switch for verbose, default is True
        :type verbose: bool
        """
        max_newt = data_ts.max_it_nl
        max_residual = np.zeros(max_newt + 1)
        self.physics.engine.n_linear_last_dt = 0
        self.timer.node['simulation'].start()
        for i in range(max_newt+1):
            self.physics.engine.assemble_linear_system(dt)  # assemble Jacobian and residual of reservoir and well blocks
            self.apply_rhs_flux(dt, t)  # apply RHS flux

            self.physics.engine.newton_residual_last_dt = self.physics.engine.calc_newton_residual()  # calc norm of residual

            max_residual[i] = self.physics.engine.newton_residual_last_dt
            counter = 0
            for j in range(i):
                if abs(max_residual[i] - max_residual[j])/max_residual[i] < data_ts.tol_sta:
                    counter += 1
            if counter > 2:
                if verbose:
                    print("Stationary point detected!")
                break

            self.physics.engine.well_residual_last_dt = self.physics.engine.calc_well_residual()
            self.physics.engine.n_newton_last_dt = i
            #  check tolerance if it converges
            if ((self.physics.engine.newton_residual_last_dt < data_ts.tol_res and
                 self.physics.engine.well_residual_last_dt < data_ts.tol_res * data_ts.tol_wel_mult) or
                    self.physics.engine.n_newton_last_dt == max_newt):
                if i > 0:  # min_i_newton
                    break
            r_code = self.physics.engine.solve_linear_equation()
            self.timer.node["newton update"].start()
            self.physics.engine.apply_newton_update(dt)
            self.timer.node["newton update"].stop()
        # End of newton loop
        converged = self.physics.engine.post_newtonloop(dt, t)

        self.timer.node['simulation'].stop()
        return converged


class ModelProperties(PropertyContainer):
    def __init__(self, phases_name, components_name, min_z=1e-11):
        # Call base class constructor
        self.nph = len(phases_name)
        Mw = np.ones(self.nph)
        super().__init__(phases_name=phases_name, components_name=components_name, Mw=Mw, min_z=min_z, temperature=1.)

    def evaluate(self, state):
        """
        Class methods which evaluates the state operators for the element based physics
        :param state: state variables [pres, comp_0, ..., comp_N-1]
        :param values: values of the operators (used for storing the operator values)
        :return: updated value for operators, stored in values
        """
        # Composition vector and pressure from state:
        vec_state_as_np = np.asarray(state)
        pressure = vec_state_as_np[0]

        zc = np.append(vec_state_as_np[1:], 1 - np.sum(vec_state_as_np[1:]))

        self.clean_arrays()
        # two-phase flash - assume water phase is always present and water component last
        for i in range(self.nph):
            self.x[i, i] = 1

        self.ph = np.array([0, 1], dtype=np.intp)


        for j in self.ph:
            # molar weight of mixture
            M = np.sum(self.x[j, :] * self.Mw)
            self.dens[j] = self.density_ev[self.phases_name[j]].evaluate(pressure)  # output in [kg/m3]
            self.dens_m[j] = self.dens[j] / M
            self.mu[j] = self.viscosity_ev[self.phases_name[j]].evaluate()  # output in [cp]

        self.nu = zc
        self.compute_saturation(self.ph)

        for j in self.ph:
            self.kr[j] = self.rel_perm_ev[self.phases_name[j]].evaluate(self.sat[j])
            self.pc[j] = 0

        return

    def evaluate_at_cond(self, pressure, zc):
        self.sat[:] = 0

        ph = np.array([0, 1], dtype=np.intp)

        for j in ph:
            self.dens_m[j] = self.density_ev[self.phases_name[j]].evaluate(1, 0)

        self.dens_m = [1025, 0.77]  # to match DO based on PVT

        self.nu = zc
        self.compute_saturation(ph)

        return self.sat, self.dens_m
