from model import Model
from darts.engines import *
import numpy as np
import meshio
from math import fabs

def run_python(m, days=0, restart_dt=0, log_3d_body_path=0, init_step = False):
    if days:
        runtime = days
    else:
        runtime = m.runtime

    mult_dt = m.params.mult_ts
    max_dt = m.params.max_ts
    m.e = m.physics.engine

    # get current engine time
    t = m.e.t

    # same logic as in engine.run
    if fabs(t) < 1e-15:
        dt = m.params.first_ts
    elif restart_dt > 0:
        dt = restart_dt
    else:
        dt = m.params.max_ts

    # evaluate end time
    runtime += t
    ts = 0

    while t < runtime:
        if init_step:   new_time = t
        else:           new_time = t + dt

        if not init_step:
            m.timer.node["update"].start()
            # store boundaries taken at previous time step
            m.reservoir.update(dt=dt, time=new_time)
            # evaluate and assign transient boundaries or sources / sinks
            m.reservoir.update_boundary_2d(dt=dt, time=new_time, physics=m.physics)
            #if res == -1:
            #    return -1
                #new_time -= dt
                #dt = m.reservoir.max_dt_change
                #continue
            # update transient boundaries or sources / sinks
            m.reservoir.update_trans(dt, m.physics.engine.X)
            m.timer.node["update"].stop()

        converged = run_timestep_python(m, dt, t)

        if converged:
            t += dt
            ts = ts + 1
            print("# %d \tT = %3g\tDT = %2g\tNI = %d\tLI=%d"
                   % (ts, t, dt, m.e.n_newton_last_dt, m.e.n_linear_last_dt))

            dt *= mult_dt
            if dt > max_dt:
               dt = max_dt

            if t + dt > runtime:
               dt = runtime - t
        else:
            new_time -= dt
            dt /= mult_dt
            print("Cut timestep to %2.3f" % dt)
            if dt < 1e-8:
               break
    # update current engine time
    m.e.t = runtime

    print("TS = %d(%d), NI = %d(%d), LI = %d(%d)" % (m.e.stat.n_timesteps_total, m.e.stat.n_timesteps_wasted,
                                                        m.e.stat.n_newton_total, m.e.stat.n_newton_wasted,
                                                        m.e.stat.n_linear_total, m.e.stat.n_linear_wasted))
def run_timestep_python(m, dt, t):
    self = m
    max_newt = self.params.max_i_newton
    self.e.n_linear_last_dt = 0
    well_tolerance_coefficient = 1e2
    self.timer.node['simulation'].start()
    res_history = []
    for i in range(max_newt + 1):
        self.e.run_single_newton_iteration(dt)
        res = self.e.calc_newton_dev()
        self.e.dev_p = res[0]
        self.e.dev_u = res[1]
        if len(res) > 2 and res[2] == res[2]:       self.e.dev_g = res[2]
        else:                                       self.e.dev_g = 0.0
        res_history.append((res[0], res[1], res[2]))

        self.e.newton_residual_last_dt = 0.0#np.sqrt(self.e.dev_u ** 2 + self.e.dev_p ** 2 + self.e.dev_g ** 2)
        #self.e.newton_residual_last_dt = self.e.calc_newton_residual()
        self.e.well_residual_last_dt = self.e.calc_well_residual()
        print(str(i) + ': ' + 'res_p = ' + str(self.e.dev_p) + '\t' + 'res_u = ' + str(self.e.dev_u) + '\t' + \
                    'res_g = ' + str(self.e.dev_g) + '\t' + 'dev_well = ' + str(self.e.well_residual_last_dt))
        self.e.n_newton_last_dt = i
        #  check tolerance if it converges
        if ((self.e.dev_p < self.params.tolerance_newton and self.e.dev_u < self.params.tolerance_newton and self.e.dev_g < self.params.tolerance_newton
           and self.e.well_residual_last_dt < well_tolerance_coefficient * self.params.tolerance_newton )
              or self.e.n_newton_last_dt == self.params.max_i_newton):
            if (i > 0):  # min_i_newton
                break

        # line search
        if False and i > 1 and res_history[-2][1] < self.params.tolerance_newton and res_history[-1][0] > 0.9 * res_history[-2][0]:
            coef = np.array([0.0, 1.0])
            history = np.array([res_history[-2], res_history[-1]])
            linear_search(self, dt, coef, history)
        else:
            r_code = self.e.solve_linear_equation()
            self.timer.node["newton update"].start()
            self.e.apply_newton_update(dt)
            self.timer.node["newton update"].stop()
            # self.reservoir.write_to_vtk('sol_poromechanics', 1, self.physics)

    # End of newton loop
    converged = self.e.post_newtonloop(dt, t)
    self.timer.node['simulation'].stop()
    return converged
def linear_search(self, dt, coef, history):
    print('LS: ' + str(coef[0]) + '\t' + 'res_p = ' + str(history[0][0]) + '\tres_u = ' + str(history[0][1]))
    print('LS: ' + str(coef[1]) + '\t' + 'res_p = ' + str(history[1][0]) + '\tres_u = ' + str(history[1][1]))
    res_history = np.array([history[0][1], history[1][1]])

    for iter in range(5):
        if coef.size > 2:
            id = res_history.argmin()
            closest_left = np.where(coef < coef[id])[0]
            closest_right = np.where(coef > coef[id])[0]
            if closest_left.size and closest_right.size:
                left = closest_left[coef[closest_left].argmax()]
                right = closest_right[coef[closest_right].argmin()]
                if res_history[left] < res_history[id]:
                    coef = np.append(coef, (coef[id] + coef[left]) / 2)
                elif res_history[right] < res_history[id]:
                    coef = np.append(coef, (coef[id] + coef[right]) / 2)
                else:
                    if res_history[left] < res_history[right]:
                        coef = np.append(coef, coef[id] - (coef[id] - coef[left]) / 4)
                    else:
                        coef = np.append(coef, coef[id] + (coef[right] - coef[id]) / 4)
            elif closest_left.size:
                left = closest_left[coef[closest_left].argmax()]
                if res_history[left] < res_history[id]:
                    coef = np.append(coef, (coef[id] + coef[left]) / 2)
                else:
                    coef = np.append(coef, coef[id] + (coef[id] - coef[left]) / 2)
            elif closest_right.size:
                right = closest_right[coef[closest_right].argmin()]
                if res_history[right] < res_history[id]:
                    coef = np.append(coef, (coef[id] + coef[right]) / 2)
                else:
                    coef = np.append(coef, coef[id] - (coef[right] - coef[id]) / 2)
            if coef[-1] <= 0: coef[-1] = 1.E-2
            if coef[-1] >= 1: coef[-1] = 1.0 - 1.E-2
        else:
            coef = np.append(coef, coef[-1] / 2)

        self.e.newton_update_coefficient = coef[-1] - coef[-2]
        self.e.apply_newton_update(dt)
        self.e.run_single_newton_iteration(dt)
        res = self.e.calc_newton_dev()
        res_history = np.append(res_history, res[1])
        print('LS: ' + str(coef[-1]) + '\t' + 'res_p = ' + str(res[0]) + '\tres_u = ' + str(res[1]))

    final_id = res_history.argmin()
    self.e.newton_update_coefficient = coef[final_id] - coef[-1]
    self.e.apply_newton_update(dt)

def just_run():
    #t0 = 0.000125
    # nt = 36 * 2
    # max_t = 365
    # t = [1.0] #np.logspace(1, np.log10(max_t), nt)
    # t = 500.0 / 86400.0 * np.ones(nt)
    # t = t0 * np.ones(nt)
    m = Model()
    m.init()
    redirect_darts_output('log.txt')
    output_directory = 'sol_{:s}'.format(m.physics_type)
    m.timer.node["update"] = timer_node()
    ith_step = 0  # Store initial conditions as ../solution0.vtk

    #t = np.array([(m.reservoir.stop_time + m.reservoir.depletion_start) / 2,
    #              m.reservoir.end_time - (m.reservoir.stop_time + m.reservoir.depletion_start) / 2]) / 86400.0
    nt = 5
    t1 = (m.reservoir.stop_time + m.reservoir.depletion_start) / 2
    ts1 = t1 / nt * np.ones(nt) / 86400
    t2 = m.reservoir.end_time
    ts2 = (t2 - t1) / nt * np.ones(nt) / 86400
    t = np.concatenate([ts1, ts2])

    ith_step = 0
    #X = np.array(m.physics.engine.X, copy=False)
    # find equilibrium
    m.reservoir.set_equilibrium()
    m.physics.engine.find_equilibrium = True
    m.params.first_ts = 1
    run_python(m, 1.0, init_step=True)
    m.reinit(output_directory)

    m.physics.engine.find_equilibrium = False
    m.reservoir.apply_geomehcanics_mode(m.physics)
    m.physics.engine.contact_solver = contact_solver.return_mapping#local_iterations#flux_from_previous_iteration#return_mapping
    m.setup_contact_friction(m.physics.engine.contact_solver)
    if m.params.linear_type == sim_params.cpu_gmres_fs_cpr:
        m.physics.engine.update_uu_jacobian()

    m.physics.engine.t = 0.0
    restart_dt = -1
    time = 0
    for ith_step, dt in enumerate(t):
        time += dt
        m.params.first_ts = dt
        m.params.max_ts = dt
        run_python(m, dt, restart_dt)
        m.reservoir.write_to_vtk(output_directory, ith_step + 1, m.physics)
        ith_step += 1
    m.print_timers()
def run_two_periods():
    #t0 = 0.000125
    # nt = 36 * 2
    # max_t = 365
    # t = [1.0] #np.logspace(1, np.log10(max_t), nt)
    # t = 500.0 / 86400.0 * np.ones(nt)
    # t = t0 * np.ones(nt)
    m = Model()
    m.init()
    redirect_darts_output('log.txt')
    output_directory = 'sol_{:s}'.format(m.physics_type)
    m.timer.node["update"] = timer_node()
    ith_step = 0  # Store initial conditions as ../solution0.vtk

    #t = np.array([(m.reservoir.stop_time + m.reservoir.depletion_start) / 2,
    #              m.reservoir.end_time - (m.reservoir.stop_time + m.reservoir.depletion_start) / 2]) / 86400.0
    nt = 2
    t1 = (m.reservoir.stop_time + m.reservoir.depletion_start) / 2
    ts1 = t1 / nt * np.ones(nt) / 86400
    t2 = m.reservoir.end_time
    ts2 = (t2 - t1) / nt * np.ones(nt) / 86400
    t = np.concatenate([ts1, ts2])

    ith_step = 0
    #X = np.array(m.physics.engine.X, copy=False)
    # find equilibrium
    m.reservoir.set_equilibrium()
    m.physics.engine.find_equilibrium = True
    m.params.first_ts = 1
    run_python(m, 1.0, init_step=True)
    m.reinit(output_directory)

    m.physics.engine.find_equilibrium = False
    m.pressure_control = False
    m.reservoir.apply_geomehcanics_mode(m.physics)
    m.physics.engine.contact_solver = contact_solver.return_mapping#local_iterations#flux_from_previous_iteration#return_mapping
    m.setup_contact_friction(m.physics.engine.contact_solver)
    if m.params.linear_type == sim_params.cpu_gmres_fs_cpr:
        m.physics.engine.update_uu_jacobian()

    m.physics.engine.t = 0.0
    restart_dt = -1
    time = 0
    for ith_step in range(nt):
        dt = t[ith_step]
        time += dt
        m.params.first_ts = dt
        m.params.max_ts = dt
        run_python(m, dt, restart_dt)
        m.reservoir.write_to_vtk(output_directory, ith_step + 1, m.physics)

    m.reservoir.apply_geomehcanics_mode_full(m.physics)
    m.pressure_control = True

    for ith_step in range(nt, 2 * nt):
        dt = t[ith_step]
        time += dt
        m.params.first_ts = dt
        m.params.max_ts = dt
        run_python(m, dt, restart_dt)
        m.reservoir.write_to_vtk(output_directory, ith_step + 1, m.physics)

    m.print_timers()
def run_two_periods_poro(mode):
    #t0 = 0.000125
    # nt = 36 * 2
    # max_t = 365
    # t = [1.0] #np.logspace(1, np.log10(max_t), nt)
    # t = 500.0 / 86400.0 * np.ones(nt)
    # t = t0 * np.ones(nt)
    m = Model(mode)
    m.init()
    redirect_darts_output('log.txt')
    output_directory = 'sol_{:s}'.format(m.physics_type)
    m.timer.node["update"] = timer_node()
    ith_step = 0  # Store initial conditions as ../solution0.vtk
    m.physics.engine.print_linear_system = True

    #t = np.array([(m.reservoir.stop_time + m.reservoir.depletion_start) / 2,
    #              m.reservoir.end_time - (m.reservoir.stop_time + m.reservoir.depletion_start) / 2]) / 86400.0
    nt = 1
    t1 = (m.reservoir.stop_time + m.reservoir.depletion_start) / 2
    ts1 = t1 / nt * np.ones(nt) / 86400
    t2 = m.reservoir.end_time
    ts2 = (t2 - t1) / nt * np.ones(nt) / 86400
    t = np.concatenate([ts1, ts2]) / 1.0
    t = np.array([3600 / 86400.0])
    #t = 10 / 86400.0 * np.ones(10)
    #nt = 10
    ith_step = 0
    #X = np.array(m.physics.engine.X, copy=False)
    # find equilibrium
    m.reservoir.set_equilibrium()
    m.physics.engine.find_equilibrium = True
    m.params.first_ts = 1
    run_python(m, 1.0, init_step=True)
    m.reinit(output_directory)

    m.physics.engine.find_equilibrium = False
    #m.reservoir.apply_geomehcanics_mode(m.physics)
    m.physics.engine.scale_rows = False#True
    m.physics.engine.contact_solver = contact_solver.RETURN_MAPPING#local_iterations#flux_from_previous_iteration#return_mapping
    m.setup_contact_friction(m.physics.engine.contact_solver)
    if m.params.linear_type == sim_params.cpu_gmres_fs_cpr:
        m.physics.engine.update_uu_jacobian()

    m.physics.engine.t = 0.0
    restart_dt = -1
    time = 0
    for ith_step in range(nt):
        dt = t[ith_step]
        time += dt
        m.params.first_ts = dt
        m.params.max_ts = dt
        run_python(m, dt, restart_dt)
        m.reservoir.write_to_vtk(output_directory, ith_step + 1, m.physics)

    # m.reservoir.apply_geomehcanics_mode_full(m.physics)
    # m.pressure_control = True
    #
    # for ith_step in range(nt, 2 * nt):
    #     dt = t[ith_step]
    #     time += dt
    #     m.params.first_ts = dt
    #     m.params.max_ts = dt
    #     run_python(m, dt, restart_dt)
    #     m.reservoir.write_to_vtk(output_directory, ith_step + 1, m.physics)

    m.print_timers()

modes = ['isolated', 'well_controlled']
run_two_periods_poro(mode='well_controlled')