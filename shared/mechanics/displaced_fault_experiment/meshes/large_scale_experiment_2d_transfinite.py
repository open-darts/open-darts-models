# ------------------------------------------------------------------------------
#
#  Gmsh Python tutorial 6
#
#  Transfinite meshes
#
# ------------------------------------------------------------------------------

import gmsh
import math
import sys
import numpy as np

def generate_large_scale_experiment_trans_grid():
    gmsh.initialize()

    gmsh.model.add("large_scale_experiment")

    # variables
    W = 20.0 / 100.0
    H = 22.0 / 100.0
    Hl = 21.5 / 100.0
    Hr = 21.7 / 100.0
    Wtot = 30 / 100.0
    Htot = 30 / 100.0
    a = 4.0 / 100.0
    b = 8.0 / 100.0
    z_dim = 20.4 / 100.0
    Lplus = b + 1.0 / 100.0
    glue_width = 0.5 / 100.0

    D = H / 2
    Y1 = D - H
    Y2 = D
    Y3 = Y1 - glue_width
    Y4 = Y2 + glue_width
    Y5 = Htot / 2 - Htot
    Y6 = Htot / 2
    phi = 70 * np.pi / 180

    lc = 0.4 / 100.0
    mult = 1.0
    mult1 = 1.0


    # gmsh.model.geo.addPoint(0, 0, 0, lc, 1)
    # gmsh.model.geo.addPoint(.1, 0, 0, lc, 2)
    # gmsh.model.geo.addPoint(.1, .3, 0, lc, 3)
    # gmsh.model.geo.addPoint(0, .3, 0, lc, 4)
    # gmsh.model.geo.addLine(1, 2, 1)
    # gmsh.model.geo.addLine(3, 2, 2)
    # gmsh.model.geo.addLine(3, 4, 3)
    # gmsh.model.geo.addLine(4, 1, 4)
    # gmsh.model.geo.addCurveLoop([4, 1, -2, 3], 1)
    # gmsh.model.geo.addPlaneSurface([1], 1)

    # add points
    y = [Y5, Y3, Y1, -b, -a, a, b, Y2, Y4, Y6]
    x = [-Wtot / 2, -(W + 2 * glue_width) / 2, -W / 2, lambda y: y / np.tan(phi), W / 2, (W + 2 * glue_width) / 2, Wtot / 2]
    for j, y_cur in enumerate(y):
        for i, x_cur in enumerate(x):
            if i != 3:
                gmsh.model.geo.addPoint(x_cur, y_cur, 0, lc, i + 10 * j + 1)
            else:
                gmsh.model.geo.addPoint(x_cur(y_cur), y_cur, 0, lc, i + 10 * j + 1)

    # 1 is 0.005
    nx_mult = 2
    nx = np.array([10, 1, 20, 20, 1, 10], dtype=np.int32)
    # 1 is 0.005
    ny_mult = 2
    ny = np.array([7, 1, 6, 8, 16, 8, 6, 1, 7], dtype=np.int32)

    # add x-lines
    for j in range(0, len(y)):
        for i in range(1, len(x)):
            gmsh.model.geo.addLine(i + 10 * j, 10 * j + i + 1, 10 * j + i)
            gmsh.model.geo.mesh.setTransfiniteCurve(10 * j + i, nx_mult * nx[i - 1] + 1)

    # add y-lines
    for j in range(0, len(y) - 1):
        for i in range(1, len(x) + 1):
            gmsh.model.geo.addLine(i + 10 * j, 10 * (j + 1) + i, 100 + 10 * j + i)
            gmsh.model.geo.mesh.setTransfiniteCurve(100 + 10 * j + i, ny_mult * ny[j] + 1)

    # add curve loops & surfaces
    surfaces = []
    for j in range(0, len(y) - 1):
        for i in range(1, len(x)):
            # [bottom, right, top, left]
            gmsh.model.geo.addCurveLoop([i + 10 * j, 100 + 10 * j + i + 1, -(10 * (j + 1) + i), -(100 + 10 * j + i)], i + 10 * j)
            gmsh.model.geo.addPlaneSurface([i + 10 * j], i + 10 * j)
            gmsh.model.geo.mesh.setTransfiniteSurface(i + 10 * j, "Left",
                                        [i + 10 * j, i + 1 + 10 * j, i + 1 + 10 * (j + 1), i + 10 * (j + 1)])
            gmsh.model.geo.mesh.setRecombine(2, i + 10 * j)
            surfaces.append((2, i + 10 * j))

    ov = gmsh.model.geo.extrude(surfaces, 0, 0, z_dim,
                                numElements=[1],
                                recombine=True)

    gmsh.model.geo.synchronize()

    LEFT = 991
    RIGHT = 992
    BOT = 993
    TOP = 994
    ZM = 995
    ZP = 996
    RES = 99991
    OVERUNDER = 99992
    RESIN = 99993
    OUTER = 99994
    FRAC = 9991
    FRAC1 = 9991
    FRAC12 = 9992
    FRAC2 = 9993
    FRAC_BOUND_STICK = 1
    FRAC_BOUND_FREE = 2

    # LEFT / RIGHT
    left = []
    right = []
    for j in range(0, len(y) - 1):
        left.append(ov[6 * (len(x) - 1) * j + 5][1])
        right.append(ov[6 * (len(x) - 1) * j + 5 * (len(x) - 1) + 3][1])
    gmsh.model.addPhysicalGroup(2, left, LEFT)
    gmsh.model.addPhysicalGroup(2, right, RIGHT)
    # BOT / TOP
    bot = []
    top = []
    for i in range(0, len(x) - 1):
        bot.append(ov[6 * i + 2][1])
        top.append(ov[6 * (len(x) - 1) * 8 + 6 * i + 4][1])
    gmsh.model.addPhysicalGroup(2, bot, BOT)
    gmsh.model.addPhysicalGroup(2, top, TOP)
    # ZM / ZP / volumes
    zm = []
    zp = []
    resin = []
    outer = []
    for j in range(0, len(y) - 1):
        for i in range(0, len(x) - 1):
            id = (len(x) - 1) * j + i
            zm.append(surfaces[id][1])
            zp.append(ov[6 * id][1])
            if i == 0 or i == len(x) - 2 or j == 0 or j == len(y) - 2:
                outer.append(ov[6 * id + 1][1])
            elif i == 1 or i == len(x) - 3 or j == 1 or j == len(y) - 3:
                resin.append(ov[6 * id + 1][1])
    res = [                                     ov[6 * (6 * 5 + 3) + 1][1],
                ov[6 * (6 * 4 + 2) + 1][1],     ov[6 * (6 * 4 + 3) + 1][1],
                ov[6 * (6 * 3 + 2) + 1][1]                                  ]
    burden = [  ov[6 * (6 * 6 + 2) + 1][1],     ov[6 * (6 * 6 + 3) + 1][1],
                ov[6 * (6 * 5 + 2) + 1][1],
                                                ov[6 * (6 * 3 + 3) + 1][1],
                ov[6 * (6 * 2 + 2) + 1][1],     ov[6 * (6 * 2 + 3) + 1][1]]

    gmsh.model.addPhysicalGroup(2, zm, ZM)
    gmsh.model.addPhysicalGroup(2, zp, ZP)

    gmsh.model.addPhysicalGroup(3, res, RES)
    gmsh.model.addPhysicalGroup(3, burden, OVERUNDER)
    gmsh.model.addPhysicalGroup(3, resin, RESIN)
    gmsh.model.addPhysicalGroup(3, outer, OUTER)
    # FRACTURE
    frac1 = [ov[6 * (6 * 4 + 2) + 3][1]]
    frac12 = [ov[6 * (6 * 3 + 2) + 3][1], ov[6 * (6 * 5 + 2) + 3][1]]
    frac2 = [ov[6 * (6 * 2 + 2) + 3][1], ov[6 * (6 * 6 + 2) + 3][1]]
    gmsh.model.addPhysicalGroup(2, frac1 + frac12 + frac2, FRAC)
    #gmsh.model.addPhysicalGroup(2, frac12, FRAC12)
    #gmsh.model.addPhysicalGroup(2, frac2, FRAC2)
    # FRACTURE BOUNDARY
    frac_free = [   124, 134, 144, 154, 164,
                    498, 630, 762, 894, 1026]
    frac_stick = [  375, 1035 ]
    gmsh.model.addPhysicalGroup(1, frac_free, FRAC_BOUND_FREE)
    gmsh.model.addPhysicalGroup(1, frac_stick, FRAC_BOUND_STICK)

    gmsh.option.setNumber("Mesh.MshFileVersion", 2.1)

    gmsh.model.mesh.generate(3)
    gmsh.write("large_scale_experiment_trans_grid.msh")

    # Launch the GUI to see the results:
    if '-nopopup' not in sys.argv:
        gmsh.fltk.run()

    gmsh.finalize()

generate_large_scale_experiment_trans_grid()