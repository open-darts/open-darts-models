W = 20.0 / 100.0;
H = 22.0 / 100.0;
Hl = 21.5 / 100.0;
Hr = 21.7 / 100.0;
Wtot = 30 / 100.0;
Htot = 30 / 100.0;
a = 4.0 / 100.0;
b = 8.0 / 100.0;
z_dim = 20.4 / 100.0;
Lplus = b + 1.0 / 100.0;
glue_width = 0.5 / 100.0;

D = H / 2;
Y1 = D - H;
Y2 = D;
Y3 = Y1 - glue_width;
Y4 = Y2 + glue_width;
Y5 = Htot / 2 - Htot;
Y6 = Htot / 2;
phi = 70 * Pi / 180;

lc = 0.4 / 100.0;//0.4 / 100.0;//;0.4 / 100.0;
mult = 1.0;
mult1 = 1.0;
// z-
Point(1) = {-W/2, Y1, 0, lc};
Point(2) = {W/2, Y1, 0, lc};
Point(3) = {W/2, -a, 0, mult * lc};
Point(4) = {W/2, b, 0, mult * lc};
Point(5) = {W/2, Y2, 0, lc};
Point(6) = {-W/2, Y2, 0, lc};
Point(7) = {-W/2, a, 0, mult * lc};
Point(8) = {-W/2, -b, 0, mult * lc};

Point(15) = {-b / Tan(phi), -b, 0, mult1 * lc};
Point(16) = {-a / Tan(phi), -a, 0, mult1 * lc};
Point(17) = {a / Tan(phi), a, 0, mult1 * lc};
Point(18) = {b / Tan(phi), b, 0, mult1 * lc};
Point(19) = {Y2 / Tan(phi), Y2, 0, mult1 * lc};
Point(20) = {Y1 / Tan(phi), Y1, 0, mult1 * lc};

Point(31) = {-(W + 2*glue_width)/2, Y3, 0, lc};
Point(32) = {(W + 2*glue_width)/2, Y3, 0, lc};
Point(35) = {(W + 2*glue_width)/2, Y4, 0, lc};
Point(36) = {-(W + 2*glue_width)/2, Y4, 0, lc};

Point(41) = {-Wtot/2, Y5, 0, lc};
Point(42) = {Wtot/2, Y5, 0, lc};
Point(45) = {Wtot/2, Y6, 0, lc};
Point(46) = {-Wtot/2, Y6, 0, lc};

Line(1) = {1,20};
Line(2) = {20,2};
Line(3) = {2,3};
Line(4) = {3,4};
Line(5) = {4,5};
Line(6) = {5,19};
Line(7) = {19,6};
Line(8) = {6,7};
Line(9) = {7,8};
Line(10) = {8,1};

Line(11) = {3, 16};
Line(12) = {4, 18};
Line(13) = {8, 15};
Line(14) = {7, 17};
Line(15) = {18, 17};
Line(16) = {17, 16};
Line(17) = {16, 15};
Line(18) = {15, 20};
Line(19) = {18, 19};

Line(31) = {31, 32};
Line(32) = {32, 35};
Line(33) = {35, 36};
Line(34) = {36, 31};

Line(41) = {41, 42};
Line(42) = {42, 45};
Line(43) = {45, 46};
Line(44) = {46, 41};

Line Loop(1) = {1, -18, -13, 10};
Line Loop(2) = {2, 3, 11, 17, 18};
Line Loop(3) = {4, 12, 15, 16, -11};
Line Loop(4) = {-16, -14, 9, 13, -17};
Line Loop(5) = {5, 6, -19, -12};
Line Loop(6) = {19, 7, 8, 14, -15};
Line Loop(7) = {31,32,33,34};
Line Loop(8) = {41,42,43,44};

Plane Surface(1) = {1};
Plane Surface(2) = {2};
Plane Surface(3) = {3};
Plane Surface(4) = {4};
Plane Surface(5) = {5};
Plane Surface(6) = {6};
Plane Surface(7) = {7,-1,-2,-3,-4,-5,-6};
Plane Surface(8) = {8,-1,-2,-3,-4,-5,-6,-7};

Recombine Surface{:};

//out[] = Extrude {0, 0, z_dim} {
//	 Surface{:};
//	 Layers{1};
//	 Recombine;
//	};

LEFT = 991;
RIGHT = 992;
BOT = 993;
TOP = 994;
ZM = 995;
ZP = 996;
RES = 99991;
OVERUNDER = 99992;
RESIN = 99993;
OUTER = 99994;
FRAC = 9991;
FRAC1 = 9991;
FRAC12 = 9992;
FRAC2 = 9993;
//FRAC_OUTER = 9992;
FRAC_LEFT = 9981;
FRAC_RIGHT = 9982;
FRAC_BOT = 9983;
FRAC_TOP = 9984;
FRAC_BOUND_STICK = 1;
FRAC_BOUND_FREE = 2;

Mesh 2;
Coherence Mesh;
Mesh.MshFileVersion = 2.1;